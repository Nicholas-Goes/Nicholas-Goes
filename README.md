<!-- Your title -->
## Hi, I'm Nicholas Goes, a Developer 🚀 from Brazil.

<!-- Your badges
You can use the website to generate badges: https://shields.io/
-->

<!-- Talking about you -->
**Talking about Personal Stuffs:**

<!-- Any image aligned to the right. Beware the width -->

- 👨🏽‍💻 I’m currently working on [FanTabs]
- 🌱 I’m currently learning Type-Script; 
- 💬 Ask me about anything, I am happy to help;
- 📫 How to reach me: nicholasfernandesdegoes@gmail.com;

**Languages and Tools:** 

[![Top Langs](https://github-readme-stats.vercel.app/api/top-langs/?username=Nicholas-Goes)](https://github.com/anuraghazra/github-readme-stats)

<p>
  <a href="https://github.com/onimur/handle-path-oz">
    <img width="55%" align="right" alt="Nicholas-Goes's github stats" src="https://github-readme-stats.vercel.app/api?username=Nicholas-Goes&show_icons=true&hide_border=true" />
  </a>
  
  <!-- Your languages and tools. Be careful with the alignment. 
  You can use this sites to get logos: https://www.vectorlogo.zone or https://simpleicons.org/
  -->
  <code><img width="10%" src="https://www.vectorlogo.zone/logos/python/python-ar21.svg"></code>
  <code><img width="10%" src="https://www.vectorlogo.zone/logos/kotlinlang/kotlinlang-ar21.svg"></code>
  <code><img width="10%" src="https://www.vectorlogo.zone/logos/typescriptlang/typescriptlang-ar21.svg"></code>
  <br />
  <code><img width="10%" src="https://www.vectorlogo.zone/logos/dartlang/dartlang-ar21.svg"></code>
  <code><img width="10%" src="https://www.vectorlogo.zone/logos/flutterio/flutterio-ar21.svg"></code>
  <code><img width="10%" src="https://upload.vectorlogo.zone/logos/nextjs/images/2d3864ef-00e0-4026-ab1d-30e4a98e2899.svg"></code>
  <br />
  <code><img width="10%" src="https://www.vectorlogo.zone/logos/laravel/laravel-ar21.svg"></code>
  <code><img width="10%" src="https://www.vectorlogo.zone/logos/angular/angular-ar21.svg"></code>
  <code><img width="10%" src="https://www.vectorlogo.zone/logos/reactjs/reactjs-ar21.svg"></code>
  <br />
  <code><img width="10%" src="https://www.vectorlogo.zone/logos/gitkraken/gitkraken-ar21.svg"></code>
  <code><img width="10%" src="https://www.vectorlogo.zone/logos/getbootstrap/getbootstrap-ar21.svg"></code>
  <code><img width="10%" src="https://www.vectorlogo.zone/logos/javascript/javascript-horizontal.svg"></code>
</p>

<!-- Your hits or visitors
site: http://hits.dwyl.com or https://visitor-badge.glitch.me
Both apis are in trouble due to the number of requests, if you know any other to register visitors, great
-->
<!-- Its main projects -->
<!--<p align="center">
  <a href="https://github.com/onimur/handle-path-oz">
    <img align="center" src="https://github-readme-stats.vercel.app/api/pin/?username=onimur&repo=handle-path-oz" />
  </a>
  <a href="https://github.com/onimur/circleci-github-changelog-generator">
    <img align="center" src="https://github-readme-stats.vercel.app/api/pin/?username=onimur&repo=circleci-github-changelog-generator" />
  </a>
</p>
-->

<!-- This readme was created by Murillo Comino - https://github.com/onimur -->
⭐️ From [Nicholas-Goes](https://github.com/Nicholas-Goes)
